# zaraVim

My portable neovim configuration for colemak-DHm keyboards.

To temporarily run on any system with Nix installed:
```
nix run gitlab:zaravi/zaravim-portable
```
