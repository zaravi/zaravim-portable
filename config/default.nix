{
  imports = [
    # General
    ./settings/options.nix
    ./settings/keymaps.nix

    # Visual enhancements
    ./colorschemes/palette.nix
    ./plugins/nvim-colorizer.nix
    ./plugins/lualine.nix

    # Files and buffers
    ./plugins/telescope.nix
    ./plugins/oil.nix
    ./plugins/harpoon.nix
    ./plugins/bufferline.nix

    # Language, Syntax and Snippets
    ./plugins/lsp.nix
    ./plugins/coq-nvim.nix
    ./plugins/treesitter.nix
    ./plugins/luasnip.nix
    ./plugins/comment.nix

    # Markdown
    # ./plugins/obsidian.nix
    ./plugins/markdown-preview.nix

    # Devel
    ./plugins/toggleterm.nix
    ./plugins/neogit.nix
    ./plugins/gitsigns.nix
    ./plugins/ollama.nix
  ];
}
