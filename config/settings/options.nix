{
  opts = {
    fileencoding = "utf-8";
    number = true;
    relativenumber = true;
    shiftwidth = 2;
  };
  type = "lua";
  viAlias = true;
  vimAlias = true;
}
