{
  plugins.coq-nvim = {
    enable = true;
    installArtifacts = true;
    settings = {
      auto_start = true;
      xdg = true;
      completion.always = true;
    };
  };
}
