{
  plugins.telescope = { 
    enable = true; 
    keymaps = {
      "<C-p>" = {
        action = "git_files";
        options.desc = "Telescope Git Files";
      };
      "<leader>fs" = "live_grep";
      "<leader>ff" = "find_files";
    };

    settings = {
      defaults = { 
        file_ignore_patterns = [ 
          "^.git/"
          "^.mypy_cache/"
          "^__pycache__/"
          "^output/"
          "^data/"
          "%.ipynb"
        ];
        layout_config = {
          prompt_position = "top";
        };
        sorting_strategy = "ascending";
      };
    };

    extensions = {
      file-browser = {
        enable = true;
      };
    };
  };
}
